# Docker Images

This project contains the Dockerfiles and the container registry for
the 40Hz group.

- remix: Debian bookworm image with PlatformIO and clang-format-16
- cpp: Alpine 3.18 with gcc/g++ and cmake
